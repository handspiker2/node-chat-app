//Imports YAY!
var socket = require("socket.io");
var express = require("express");

var server = express.createServer();
var io = socket.listen(server);

server.use("/", express.static(__dirname + '/public'));
server.use("/public", express.static(__dirname + '/public'));

io.set("log level",1);

io.sockets.on("connection", function(client){
	console.log("New Connection!");
	//Get the join
	client.on("join", function(name){
		console.log(name + " JOINED");
		client.broadcast.emit("joined", name);
		client.emit("joined", "You");
	});

	client.on("message", function(data){
		console.log(data.name + ": " + data.message);
		client.broadcast.emit("message", data.name + ": " + data.message);
		client.emit("message", "YOU: " + data.message);
	});
});



server.listen(8080);